import React, { Component, Fragment } from "react";
import { Col, Container, Row, Table, Form, Button } from "react-bootstrap";

import Input from "../components/Input";
import FilterBy from "../components/FilterBy";

export default class Player extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [
        {
          id: 1,
          username: "Kadap",
          email: "kadap@gmail.com",
          exp: 1500,
          lvl: 1,
        },
        {
          id: 2,
          username: "Tarang",
          email: "tarang@gmail.com",
          exp: 2700,
          lvl: 2,
        },
        {
          id: 3,
          username: "Yes",
          email: "yes@gmail.com",
          exp: 4500,
          lvl: 4,
        },
      ],
      id: null,
      username: "",
      email: "",
      exp: "",
      lvl: "",
    };
  }

  handleChange = (key, value) => {
    this.setState({
      [key]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { data, username, email, exp, lvl, id } = this.state;

    if (id === "") {
      this.setState({
        data: [
          ...data,
          {
            id: data.length + 1,
            username,
            email,
            exp,
            lvl,
          },
        ],
      });
    } else {
      const NotChosenPlayer = data
        .filter((player) => player.id !== id)
        .map((filterPlayer) => {
          return filterPlayer;
        });

      this.setState({
        data: [
          ...NotChosenPlayer,
          {
            id,
            username,
            email,
            exp,
            lvl,
          },
        ],
      });
    }
  };

  editData = (id) => {
    const chosenPlayer = this.state.data
      .filter((player) => player.id === id)
      .map((filterPlayer) => {
        return filterPlayer;
      });
    this.setState({
      id: chosenPlayer[0].id,
      username: chosenPlayer[0].username,
      email: chosenPlayer[0].email,
      exp: chosenPlayer[0].exp,
      lvl: chosenPlayer[0].lvl,
    });
  };

  deleteData = (id) => {
    const newPlayerList = this.state.data
      .filter((player) => player.id !== id)
      .map((filterPlayer) => {
        return filterPlayer;
      });

    this.setState({
      data: newPlayerList,
    });
  };

  usernameList = (value) => this.setState({ usernameFilter: value });
  emailList = (value) => this.setState({ emailFilter: value });
  expList = (value) => this.setState({ expFilter: value });
  lvlList = (value) => this.setState({ lvlFilter: value });
  noneList = (value) => this.setState({ noneFilter: value });

  saveFilter = (event) => this.setState({ by: event.target.value });

  filterPlayers = () => {
    const { players, usernameFilter, emailFilter, expFilter, lvlFilter, by } =
      this.state;

    if (players && players.length > 0) {
      switch (by) {
        case "username":
          return players.filter((player) =>
            player.username.toLowerCase().includes(usernameFilter.toLowerCase())
          );
        case "email":
          return players.filter((player) =>
            player.email.toLowerCase().includes(emailFilter.toLowerCase())
          );
        case "exp":
          return players.filter((player) => player.exp === expFilter);

        case "lvl":
          return players.filter((player) => player.lvl === lvlFilter);

        case "none":
          return players;
        default:
          return players;
      }
    } else {
      return [];
    }
  };

  render() {
    const { data, username, email, exp, lvl, by } = this.state;

    return (
      <Fragment>
        <Container className="mt-5">
          <Input saveFilter={this.saveFilter} />
          <FilterBy
            by={by}
            usernameList={this.usernameList}
            emailList={this.emailList}
            expList={this.expList}
            lvlList={this.lvlList}
          />
          <Row>
            <Col>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>EXP</th>
                    <th>LVL</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((player, index) => {
                    return (
                      <tr>
                        <td>{index + 1}</td>
                        <td>{player.username}</td>
                        <td>{player.email}</td>
                        <td>{player.exp}</td>
                        <td>{player.lvl}</td>
                        <td>
                          <Button
                            onClick={() => this.editData(player.id)}
                            variant="primary"
                          >
                            Edit
                          </Button>

                          <Button
                            onClick={() => this.deleteData(player.id)}
                            variant="danger"
                          >
                            Delete
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </Col>
            <Col>
              <Form onSubmit={this.handleSubmit}>
                <Form.Group controlId="username">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter your username"
                    value={username}
                    onChange={(event) =>
                      this.handleChange("username", event.target.value)
                    }
                  />
                </Form.Group>

                <Form.Group controlId="email">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter your email"
                    value={email}
                    onChange={(event) =>
                      this.handleChange("email", event.target.value)
                    }
                  />
                </Form.Group>

                <Form.Group controlId="exp">
                  <Form.Label>Experience</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter your EXP point"
                    value={exp}
                    onChange={(event) =>
                      this.handleChange("exp", event.target.value)
                    }
                  />
                </Form.Group>

                <Form.Group controlId="lvl">
                  <Form.Label>Level</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter your latest Level"
                    value={lvl}
                    onChange={(event) =>
                      this.handleChange("lvl", event.target.value)
                    }
                  />
                </Form.Group>

                <Button className="mt-3" variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </Fragment>
    );
  }
}
