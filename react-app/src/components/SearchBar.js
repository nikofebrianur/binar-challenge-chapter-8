import React, { Component } from "react";

export default class SearchBar extends Component {
  render() {
    return (
      <Form inline>
        <FormControl type="text" placeholder="Search Player" className="mr-sm-2" />
        <Button variant="outline-success">Search</Button>
      </Form>
    );
  }
}
