import React from "react";

const FilterUsername = ({ usernameList }) => (
  <div>
    <label>Username:</label>
    <input
      type="text"
      name="username"
      onChange={(event) => usernameList(event.target.value)}
    />
  </div>
);

const FilterEmail = ({ emailList }) => (
  <div>
    <label>Email:</label>
    <input
      type="email"
      name="email"
      onChange={(event) => emailList(event.target.value)}
    />
  </div>
);

const FilterExp = ({ expList }) => (
  <div>
    <label>EXP:</label>
    <input
      type="number"
      name="exp"
      onChange={(event) => expList(event.target.value)}
    />
  </div>
);

const FilterLevel = ({ lvlList }) => (
  <div>
    <label>Level:</label>
    <input
      type="number"
      name="lvl"
      onChange={(event) => lvlList(event.target.value)}
    />
  </div>
);

const FilterBy = ({
  by,
  usernameList,
  emailList,
  expList,
  lvlList,
  noneList,
}) => {
  switch (by) {
    case "username":
      return <FilterUsername usernameList={usernameList}></FilterUsername>;
    case "email":
      return <FilterEmail emailList={emailList}></FilterEmail>;
    case "exp":
      return <FilterExp expList={expList}></FilterExp>;
    case "lvl":
      return <FilterLevel lvlList={lvlList}></FilterLevel>;
    case "none":
      return null;
    default:
      return null;
  }
};

export default FilterBy;
