import React from 'react'

const Input = ({ saveFilter }) => (
    <div>
        <label> Filter by:</label>
        <select onChange={(event) => saveFilter(event)}>
            <option value="none">None</option>
            <option value="username">Username</option>
            <option value="email">Email</option>
            <option value="exp">EXP</option>
            <option value="level">Level</option>
        </select>
    </div>
);

export default Input